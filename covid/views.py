from django.shortcuts import render, redirect
from covid.models import Covid
from covid.forms import ColdSymptomsLogForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def my_cold_symptoms(request): 
    cold = Covid.objects.filter(patient=request.user)
    context = {
        "my_cold_symptoms_log": cold, 
    }

    return render(request, "covid/list.html", context)

@login_required
def create_cold_symptom_entry(request): 
    if request.method == "POST":
        form = ColdSymptomsLogForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("my_cold_symptom_log")
    else:
        form = ColdSymptomsLogForm()

    context = {
        "form": form,
    }

    return render(request, "covid/create.html", context)