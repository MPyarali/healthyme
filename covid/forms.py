from django.forms import ModelForm
from covid.models import Covid

class ColdSymptomsLogForm(ModelForm): 
    class Meta: 
        model = Covid
        fields = [
        "date", 
        "temperature", 
        "cough", 
        "sore_throat", 
        "headache", 
        "loss_of_smell", 
        "difficulty_breathing", 
        "hospitalized", 
        "took_test", 
        "test_type",
        "test_result", 
        "patient",  
        ] 