from django.contrib import admin
from covid.models import Covid

# Register your models here.
@admin.register(Covid)
class CovidAdmin(admin.ModelAdmin): 
    list_display = [ 
        "date", 
        "temperature", 
        "cough", 
        "sore_throat", 
        "headache", 
        "loss_of_smell", 
        "difficulty_breathing", 
        "hospitalized", 
        "took_test", 
        "test_type",
        "test_result",  
    ]