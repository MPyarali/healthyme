from django.urls import path
from covid.views import my_cold_symptoms, create_cold_symptom_entry

urlpatterns = [
    path("uri/create/", create_cold_symptom_entry, name="create_cold_symptom_entry"),
    path("uri/log/", my_cold_symptoms, name="my_cold_symptoms_log"),
]
