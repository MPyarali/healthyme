from django.db import models
from django.conf import settings 

# Create your models here.
class Covid(models.Model): 
    CHOICES = (
        ('Antigen', 'At home rapid antigen test kit'), 
        ('PCR Home', 'PCR test done at home'), 
        ('PCR Clinic', 'PCR test done by healthcare provider'), 
        ('Combined', 'Combined COVID/influenza test'), 
    )
    TEST = (
        ('POSITIVE', 'POSITIVE'), 
        ('NEGATIVE', 'NEGATIVE'),
    )

    date = models.DateTimeField() 
    temperature = models.CharField(max_length =4, blank=True)
    cough = models.BooleanField(default=False)
    sore_throat = models.BooleanField(default=False)
    headache = models.BooleanField(default=False)
    loss_of_smell = models.BooleanField(default=False)
    difficulty_breathing = models.BooleanField(default=False)
    hospitalized = models.BooleanField(default=False)
    took_test = models.BooleanField(default=False, blank=True)
    test_type = models.CharField(max_length=15, choices=CHOICES, blank=True)
    test_result = models.CharField(max_length=15, choices=TEST, blank=True)
    patient = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="covid",
        on_delete=models.CASCADE,
        null=True,
    )

    class Meta: 
        ordering = ["date"]

