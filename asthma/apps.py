from django.apps import AppConfig


class AsthmaConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "asthma"
