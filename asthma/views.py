from django.shortcuts import render, redirect
from asthma.models import Asthma
from asthma.forms import AsthmaLogForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def my_asthma(request): 
    asthma = Asthma.objects.filter(patient=request.user)
    context = {
        "my_asthma_log": asthma, 
    }

    return render(request, "asthma/list.html", context)

@login_required
def create_asthma_entry(request): 
    if request.method == "POST":
        form = AsthmaLogForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("my_asthma_log")
    else:
        form = AsthmaLogForm()

    context = {
        "form": form,
    }

    return render(request, "asthma/create.html", context)