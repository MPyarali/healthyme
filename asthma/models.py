from django.db import models
from django.conf import settings 

# Create your models here.
class Asthma(models.Model): 
    CHOICES = (
        ('Great', 'No wheezing/ coughing'), 
        ('Good', 'Slight wheezing/ coughing'), 
        ('Alright', 'Woke up 2-3x'), 
        ('Terrible', 'Awake all night'), 
    )

    date = models.DateTimeField() 
    wheezing = models.BooleanField(default=False)
    chest_tightness = models.BooleanField(default=False)
    shortness_of_breath = models.BooleanField(default=False)
    cough = models.BooleanField(default=False)
    missed_activities = models.BooleanField(default=False)
    sleep = models.CharField(max_length=15, choices=CHOICES)
    took_medications = models.BooleanField(default=False)
    used_inhaler = models.BooleanField(default=False)
    asthma_attack = models.BooleanField(default=False)
    patient = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="asthma",
        on_delete=models.CASCADE,
        null=True,
    )

    class Meta: 
        ordering = ["date"]

