from django.forms import ModelForm
from asthma.models import Asthma

class AsthmaLogForm(ModelForm): 
    class Meta: 
        model = Asthma
        fields = [
        "date", 
        "wheezing", 
        "chest_tightness", 
        "shortness_of_breath", 
        "cough", 
        "missed_activities", 
        "sleep", 
        "took_medications", 
        "used_inhaler", 
        "asthma_attack", 
        "patient", 
        ] 