from django.urls import path
from asthma.views import my_asthma, create_asthma_entry

urlpatterns = [
    path("asthma/create/", create_asthma_entry, name="create_asthma_entry"),
    path("asthma/log/", my_asthma, name="my_asthma_log"),
]
