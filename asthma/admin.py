from django.contrib import admin
from asthma.models import Asthma 

# Register your models here.
@admin.register(Asthma)
class AsthmaAdmin(admin.ModelAdmin): 
    list_display = [ 
        "date", 
        "wheezing", 
        "chest_tightness", 
        "shortness_of_breath", 
        "cough", 
        "missed_activities", 
        "sleep", 
        "took_medications", 
        "used_inhaler", 
        "asthma_attack", 
    ]