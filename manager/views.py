from django.shortcuts import render
from manager.models import Manager

# Create your views here.
def list_managers(request): 
    managers = [
        Manager("Physician Info", "A place to store information about your medical team", "https://media.istockphoto.com/id/1139549801/vector/stethoscope-heart-icon.jpg?s=612x612&w=0&k=20&c=qEJ7fFxWkok8j7FYYj4NwAlHSgqsw-azZz7c3IQJ4KI=", "providers/log/"),
        Manager("Medications", "A place to store information about all of the medications prescribed to you", "https://media.istockphoto.com/id/828331166/vector/prescription-bottle.jpg?s=612x612&w=0&k=20&c=CSs5I-dNhTkN7A32Q88RLyio9XwdmFpR23-xUhDJyPE=", "medications/log/"),
        Manager("Blood Pressure", "A place to store your daily blood pressure measurements", "https://www.shutterstock.com/image-vector/blood-pressure-icon-heart-sphygmomanometer-260nw-1758198341.jpg", "bp/log/"),
        Manager("Blood Glucose", "A place to store your daily blood glucose measurements", "https://img.freepik.com/premium-vector/beautiful-vector-diabetic-icon-glucometer-cartoon-sign-blood-glucose-meter-pictogram-medical-editable-illustration-isolated-white-background_505557-3805.jpg", "glucose/log/"),
        Manager("COVID/ Cold Symptoms", "A place to keep track of your cold and COVID symptoms", "https://illustoon.com/photo/7462.png", "uri/log/"),
        Manager("Asthma", "A place to keep track of your asthma symptoms", "https://www.mydiversepatients.com/assets/img/app_asthma_small.png", "asthma/log/"),
    ]

    context = {
        "managers_list": managers,
    }
    return render(request, "manager/list.html", context)