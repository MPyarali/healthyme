from django.urls import path 
from manager.views import list_managers
from asthma.views import my_asthma
from covid.views import my_cold_symptoms
from bg.views import my_blood_glucose
from bp.views import my_blood_pressure
from meds.views import my_medications
from physician.views import my_medical_team


urlpatterns = [
    path("", list_managers, name="list_managers"),
    path("asthma/log/", my_asthma, name="asthma/log/"),
    path("uri/log/", my_cold_symptoms, name="uri/log/"),
    path("glucose/log/", my_blood_glucose, name="glucose/log/"),
    path("bp/log/", my_blood_pressure, name="bp/log/"),
    path("medications/log/", my_medications, name="medications/log/"),
    path("providers/log/", my_medical_team, name="providers/log/"),
]