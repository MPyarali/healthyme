from django.db import models
from django.conf import settings 

# Create your models here.
class Manager():
    def __init__(self, title, description, picture, link):
        self.title = title
        self.description = description
        self.picture = picture
        self.link = link

    def __str__(self): 
        return self.title