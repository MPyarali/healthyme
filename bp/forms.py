from django.forms import ModelForm
from bp.models import Pressures

class PressuresLogForm(ModelForm): 
    class Meta: 
        model = Pressures
        fields = [
        "date", 
        "systolic", 
        "diastolic", 
        "heart_rate",
        "notes", 
        "patient", 
        ] 