from django.db import models
from django.conf import settings 

# Create your models here.
class Pressures(models.Model): 
    date = models.DateTimeField() 
    systolic = models.CharField(max_length=4, blank=True)
    diastolic = models.CharField(max_length=4, blank=True)
    heart_rate = models.CharField(max_length=4, blank=True)
    notes = models.TextField(blank=True) 
    patient = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="pressures",
        on_delete=models.CASCADE,
        null=True,
    )

    class Meta: 
        ordering = ["date"]
