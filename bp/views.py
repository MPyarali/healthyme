from django.shortcuts import render, redirect
from bp.models import Pressures
from bp.forms import PressuresLogForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def my_blood_pressure(request): 
    pressures = Pressures.objects.filter(patient=request.user)
    context = {
        "my_blood_pressure_log": pressures, 
    }

    return render(request, "bp/list.html", context)

@login_required
def create_pressure_entry(request): 
    if request.method == "POST":
        form = PressuresLogForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("my_blood_pressure_log")
    else:
        form = PressuresLogForm()

    context = {
        "form": form,
    }

    return render(request, "bp/create.html", context)

