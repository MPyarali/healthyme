from django.contrib import admin
from bp.models import Pressures

# Register your models here.
@admin.register(Pressures)
class PressuresAdmin(admin.ModelAdmin): 
    list_display = [ 
      "date", 
        "systolic", 
        "diastolic", 
        "heart_rate",
        "notes", 
        ] 