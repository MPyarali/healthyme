from django.urls import path
from bp.views import my_blood_pressure, create_pressure_entry

urlpatterns = [
    path("bp/create/", create_pressure_entry, name="create_pressure_entry"),
    path("bp/log/", my_blood_pressure, name="my_blood_pressure_log"),
]
