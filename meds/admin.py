from django.contrib import admin
from meds.models import Medications

# Register your models here.
@admin.register(Medications)
class MedicationsAdmin(admin.ModelAdmin): 
    list_display = [ 
        "name", 
        "indication", 
        "pill_appearance", 
        "how_many", 
        "how_often", 
        "how_to_take", 
        "start_date", 
        "id", 
    ]


