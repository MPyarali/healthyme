from django.db import models
from django.conf import settings 
from physician.models import MedicalTeam

# Create your models here.
class Medications(models.Model): 
    name = models.CharField(max_length=200)
    indication = models.CharField(max_length=300)
    pill_appearance = models.CharField(max_length=300, blank=True)
    how_many = models.CharField(max_length=4)
    how_often = models.CharField(max_length=4)
    how_to_take = models.CharField(max_length=300, blank=True)
    start_date = models.DateTimeField(blank=True)
    prescribed_by = models.ForeignKey(
        MedicalTeam, 
        related_name = "medications",
        on_delete = models.CASCADE, 
        null=True
    )
    patient = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="medications",
        on_delete=models.CASCADE,
        null=True,
    )

    class Meta: 
        ordering = ["-start_date"]

