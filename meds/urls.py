from django.urls import path
from meds.views import my_medications, show_medication, create_medication_entry, edit_medication_entry

urlpatterns = [
    path("medications/create/", create_medication_entry, name="create_medication_entry"),
    path("medications/log/", my_medications, name="my_medications"),
    path("medications/<int:id>/", show_medication, name="show_medication"), 
    path("medications/<int:id>/edit/", edit_medication_entry, name="edit_medication_entry"),
]
