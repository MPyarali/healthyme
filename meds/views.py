from django.shortcuts import render, redirect, get_object_or_404
from meds.models import Medications
from meds.forms import MedicationsForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def my_medications(request): 
    medications = Medications.objects.filter(patient=request.user)
    context = {
        "my_medications": medications, 
    }

    return render(request, "meds/list.html", context)

@login_required
def create_medication_entry(request): 
    if request.method == "POST":
        form = MedicationsForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("my_medications")
    else:
        form = MedicationsForm()

    context = {
        "form": form,
    }

    return render(request, "meds/create.html", context)

@login_required
def show_medication(request, id): 
    medication = get_object_or_404(Medications, id=id)
    context = { 
        "medication": medication, 
    }
    return render(request, "meds/detail.html", context)

@login_required
def edit_medication_entry(request, id): 
    medication_instance=get_object_or_404(Medications, id=id)
    if request.method == "POST": 
       form = MedicationsForm(request.POST, instance=medication_instance)
       if form.is_valid(): 
            form.save() 
            return redirect ("show_medication", id=id)
    else: 
        form = MedicationsForm(instance=medication_instance) 

    context = { 
        "my_medications": medication_instance, 
        "form": form, 
    }

    return render(request, "meds/edit.html", context)
