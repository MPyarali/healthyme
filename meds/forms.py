from django.forms import ModelForm
from meds.models import Medications

class MedicationsForm(ModelForm): 
    class Meta: 
        model = Medications
        fields = [
        "name", 
        "indication", 
        "pill_appearance", 
        "how_many", 
        "how_often", 
        "how_to_take", 
        "start_date", 
        "prescribed_by",
        "patient",  
        ] 
