from django.contrib import admin
from bg.models import Glucose

# Register your models here.
@admin.register(Glucose)
class GlucoseAdmin(admin.ModelAdmin): 
    list_display = [ 
        "date", 
        "time_of_day", 
        "before", 
        "after", 
        "notes", 
    ]