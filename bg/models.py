from django.db import models
from django.conf import settings 

# Create your models here.
class Glucose(models.Model): 
    CHOICES = (
        ('Breakfast', 'Breakfast'), 
        ('Lunch', 'Lunch'), 
        ('Dinner', 'Dinner'), 
        ('Bedtime', 'Bedtime'), 
    )
    date = models.DateTimeField() 
    time_of_day = models.CharField(max_length=15, choices=CHOICES)
    before = models.CharField(max_length=4, blank=True)
    after = models.CharField(max_length=4, blank=True)
    notes = models.TextField(blank=True) 
    patient = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="glucose",
        on_delete=models.CASCADE,
        null=True,
    )

    class Meta: 
        ordering = ["date"]

