from django.forms import ModelForm
from bg.models import Glucose

class GlucoseLogForm(ModelForm): 
    class Meta: 
        model = Glucose
        fields = [
        "date", 
        "time_of_day", 
        "before", 
        "after", 
        "notes", 
        "patient", 
        ] 