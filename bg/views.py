from django.shortcuts import render, redirect
from bg.models import Glucose
from bg.forms import GlucoseLogForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def my_blood_glucose(request): 
    glucose = Glucose.objects.filter(patient=request.user)
    context = {
        "my_glucose_log": glucose, 
    }

    return render(request, "bg/list.html", context)

@login_required
def create_glucose_entry(request): 
    if request.method == "POST":
        form = GlucoseLogForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("my_glucose_log")
    else:
        form = GlucoseLogForm()

    context = {
        "form": form,
    }

    return render(request, "bg/create.html", context)
