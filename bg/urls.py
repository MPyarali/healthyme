from django.urls import path
from bg.views import my_blood_glucose, create_glucose_entry

urlpatterns = [
    path("glucose/create/", create_glucose_entry, name="create_glucose_entry"),
    path("glucose/log/", my_blood_glucose, name="my_glucose_log"),
]
