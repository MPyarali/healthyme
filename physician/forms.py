from django.forms import ModelForm
from physician.models import MedicalTeam

class MedicalTeamForm(ModelForm): 
    class Meta: 
        model = MedicalTeam
        fields = [
            "provider_name", 
            "specialty", 
            "phone_number", 
            "office_location", 
            "next_appt", 
            "notes", 
            "patient", 
        ] 
