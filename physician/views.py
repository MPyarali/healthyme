from django.shortcuts import render, redirect, get_object_or_404
from physician.models import MedicalTeam
from physician.forms import MedicalTeamForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def my_medical_team(request): 
    medical_team = MedicalTeam.objects.filter(patient=request.user)
    context = {
        "my_medical_team": medical_team, 
    }

    return render(request, "physician/list.html", context)

@login_required
def create_provider_entry(request): 
    if request.method == "POST":
        form = MedicalTeamForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("my_medical_team")
    else:
        form = MedicalTeamForm()

    context = {
        "form": form,
    }

    return render(request, "physician/create.html", context)

@login_required
def show_provider(request, id): 
    provider = get_object_or_404(MedicalTeam, id=id)
    context = { 
        "provider": provider, 
    }
    return render(request, "physician/detail.html", context)

@login_required
def edit_provider_entry(request, id): 
    provider_instance=get_object_or_404(MedicalTeam, id=id)
    if request.method == "POST": 
       form = MedicalTeamForm(request.POST, instance=provider_instance)
       if form.is_valid(): 
            form.save() 
            return redirect ("show_provider", id=id)
    else: 
        form = MedicalTeamForm(instance=provider_instance) 

    context = { 
        "my_medical_team": provider_instance, 
        "form": form, 
    }

    return render(request, "physician/edit.html", context)
