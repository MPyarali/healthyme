from django.db import models
from django.conf import settings 
from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.
class MedicalTeam(models.Model): 
    provider_name = models.CharField(max_length=200)
    specialty = models.CharField(max_length=200)
    phone_number = PhoneNumberField(blank=True)
    office_location = models.CharField(max_length=200, blank=True)
    next_appt = models.DateTimeField(null=True, blank=True)
    notes = models.TextField(blank=True) 
    patient = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="provider",
        on_delete=models.CASCADE,
        null=True,
    )

    class Meta: 
        ordering = ["provider_name"]