from django.contrib import admin
from physician.models import MedicalTeam

# Register your models here.
@admin.register(MedicalTeam)
class MedicalTeamAdmin(admin.ModelAdmin): 
    list_display = [ 
        "provider_name", 
        "specialty", 
        "phone_number", 
        "office_location", 
        "next_appt", 
        "notes", 
        "id", 
    ]