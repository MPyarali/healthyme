from django.urls import path
from physician.views import my_medical_team, create_provider_entry, edit_provider_entry, show_provider

urlpatterns = [
    path("providers/create/", create_provider_entry, name="create_provider_entry"),
    path("providers/log/", my_medical_team, name="my_medical_team"),
    path("providers/<int:id>/", show_provider, name="show_provider"), 
    path("providers/<int:id>/edit/", edit_provider_entry, name="edit_provider_entry"),
]
